#include "config.hpp"
#include "main/main.hpp"
#include "main/tasks.hpp"
#include <Encoder.hpp>
#include <FreeRTOS.h>
#include <I2C.hpp>
#include <IMUDataProcessor.hpp>
#include <OpticalEncoder.hpp>
#include <UART.hpp>
#include <functional>
#include <map>
#include <task.h>
// #include <tusb.h>

queue_t odometers_data_queue;

static std::map<uint, std::function<bool(void)>> callbacks;

const auto slowTimerID = 1U;
bool slowTimerHandler(repeating_timer_t *rt) {
  callbacks.at(slowTimerID)();
  return true;
};
const auto fastTimerID = 2U;
bool fastTimerHandler(repeating_timer_t *rt) {
  callbacks.at(fastTimerID)();
  return true;
};

void main_core1(void) {

  // Setup Hardware
  auto rightEncoder = Encoder::Builder()
                          .at(config::rightEncoderPin)
                          .onEvents(IPin::RISING)
                          .withStep(config::encoderStep)
                          .build();

  auto leftEncoder = Encoder::Builder()
                         .at(config::leftEncoderPin)
                         .onEvents(IPin::RISING)
                         .withStep(config::encoderStep)
                         .build();

  auto i2cPtr = I2C::Builder()
                    .withSDA(config::SDAPin)
                    .withSCL(config::SCLPin)
                    .withFrequency(config::I2CFrequency)
                    .get();

  auto mpu = MPU6500::Builder()
                 .withAccelScale(MPU6500::A2)
                 .withGyroScale(MPU6500::G250)
                 .withI2C(std::move(i2cPtr))
                 .get();

  auto IMU = IMUDataProcessor(std::move(mpu));

  // Setup Tasks

  // Slow Timer
  auto slowTimerCallback = [&rightEncoder, &leftEncoder, &IMU]() {
    // Odometer data acquisition
    // Get data here ...
    auto deltaRightPosition = rightEncoder.getDeltaAngle();
    auto deltaLeftPosition = leftEncoder.getDeltaAngle();

    // Odometer data processing
    // Process data here ...
    float deltaPosition =
        (deltaRightPosition + deltaLeftPosition) * config::wheelRadius / 2.0;

    float deltaRotation = IMU.getDeltaAngle();

    // Create Payload
    auto payload = std::pair<float, float>(deltaPosition, deltaRotation);

    // Writes to inter process queue pair(deltaX, deltaTheta)
    queue_try_add(&odometers_data_queue, &payload);

    // Signals RTOS to read queue
    notifyTaskFromISR(TaskHandleID::QUEUE_RECEIVE);
    return true;
  };

  // Debug Led (Core 1 is Alive Signal)
  const auto led = Pin(PICO_DEFAULT_LED_PIN);

  // Fast Timer
  auto fastTimerCallback = [&led, &IMU]() {
    IMU.update();

    led.toggle();
    return true;
  };

  // Repeating Timer Setup
  // Setup Pool so Timer interrupt runs on this core
  auto pool = alarm_pool_create(1, 2);

  repeating_timer_t slowTimer;
  callbacks.insert({slowTimerID, slowTimerCallback});
  alarm_pool_add_repeating_timer_ms(pool, config::slowTimerTimeout,
                                    slowTimerHandler, nullptr, &slowTimer);

  repeating_timer_t fastTimer;
  callbacks.insert({fastTimerID, fastTimerCallback});
  alarm_pool_add_repeating_timer_us(pool, config::fastTimerTimeout,
                                    fastTimerHandler, nullptr, &fastTimer);

  // Init inter process queue with size of 32 odometer data pairs
  queue_init(&odometers_data_queue, sizeof(std::pair<float, float>), 32);

  // USB init and task (disabled)
  // tusb_init();
  while (true) {
    // tuh_task();
  };
}
