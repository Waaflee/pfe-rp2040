#include "main/tasks.hpp"
#include "main/main.hpp"
#include <DifferentialRobot.hpp>
#include <MessageParser.hpp>
#include <PID.hpp>
#include <Pin.hpp>
#include <Store.hpp>
#include <UART.hpp>
#include <queue.h>

TaskHandle_t prvQueueReceiveTaskHandle;
TaskHandle_t prvTraslationControlTaskHandle;
TaskHandle_t prvRotationControlTaskHandle;
TaskHandle_t prvCalibrateSensorsTaskHandle;
TaskHandle_t prvProcessIncomingMessageTaskHandle;
TaskHandle_t prvReportMovementTaskHandle;

void notifyTask(TaskHandleID task) {
  Store::instance().getUart().writeline("====");
  Store::instance().getUart().writeline("notify: " +
                                        std::to_string((uint)task));
  Store::instance().getUart().writeline("====");

  xTaskNotify(*allHandles[static_cast<uint>(task)], 0, eNoAction);
}
void notifyTaskFromISR(TaskHandleID task) {
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;
  xTaskNotifyFromISR(*allHandles[static_cast<uint>(task)], 0, eNoAction,
                     &xHigherPriorityTaskWoken);
}

static void prvQueueReceiveTask(void *pvParameters);
static void prvTraslationControlTask(void *pvParameters);
static void prvRotationControlTask(void *pvParameters);
static void prvCalibrateSensorsTask(void *pvParameters);
static void prvProcessIncomingMessageTask(void *pvParameters);
static void prvReportMovementTask(void *pvParameters);

static void prvLifeSignalTask(void *pvParameters);

QueueHandle_t xDeltaReportsQueue = NULL;

void vSetupQueues() {
  xDeltaReportsQueue = xQueueCreate(5, sizeof(std::pair<float, float>));
}

void vSetupTasks() {
  vSetupQueues();
  // Inter Process Communication Task
  xTaskCreate(prvQueueReceiveTask, "ReadQueue", configMINIMAL_STACK_SIZE, NULL,
              tskIDLE_PRIORITY + 2, &prvQueueReceiveTaskHandle);
  //   Traslation Control Task
  xTaskCreate(prvTraslationControlTask, "TraslationPid",
              configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 3,
              &prvTraslationControlTaskHandle);
  //   Rotation Control Task
  xTaskCreate(prvRotationControlTask, "RotationPid", configMINIMAL_STACK_SIZE,
              NULL, tskIDLE_PRIORITY + 3, &prvRotationControlTaskHandle);
  //   Sensor Calibration Task
  xTaskCreate(prvCalibrateSensorsTask, "CalibrateSensors",
              configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1,
              &prvCalibrateSensorsTaskHandle);
  //   Process Incoming Message Task
  xTaskCreate(prvProcessIncomingMessageTask, "ProcessIncomingMessage",
              configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1,
              &prvProcessIncomingMessageTaskHandle);
  //   Report new movement
  xTaskCreate(prvReportMovementTask, "ReportMovement", configMINIMAL_STACK_SIZE,
              NULL, tskIDLE_PRIORITY + 1, &prvReportMovementTaskHandle);
  // Life Signal Pulse
  xTaskCreate(prvLifeSignalTask, "LifeSignal", configMINIMAL_STACK_SIZE / 4,
              NULL, tskIDLE_PRIORITY + 1, nullptr);
}

static void prvLifeSignalTask(void *pvParameters) {
  const auto debugLed = Pin(15);
  while (true) {
    vTaskDelay(500);
    debugLed.toggle();
  }
};

static void prvQueueReceiveTask(void *pvParameters) {
  auto &store = Store::instance();
  auto &uart = store.getUart();

  while (true) {
    xTaskNotifyWait(0x00,    /* Don't clear any notification bits on entry. */
                    0,       /* Reset the notification value to 0 on exit. */
                    nullptr, /* Notified value pass out in
                                         ulNotifiedValue. */
                    portMAX_DELAY); /* Block indefinitely. */
    std::pair<float, float> payload;
    if (queue_try_remove(&odometers_data_queue, &payload)) {
      uart.writeline("Reading Queue!");

      auto &[x, o] = payload;

      store.updateCurrentRotPos(payload);

      uart.writeline("Current State: " + std::to_string(store.getState()));
      uart.writeline("Current x error: " +
                     std::to_string(store.getPositionDiff()));
      uart.writeline("Current o error: " +
                     std::to_string(store.getRotationDiff()));
      uart.writeline("");
      uart.writeline("");

      auto success =
          xQueueSend(xDeltaReportsQueue, (void *)&payload, (TickType_t)0);

    } else {
      uart.writeline("Queue is empty!");
    }
  };
}
static void prvTraslationControlTask(void *pvParameters) {

  auto &store = Store::instance();
  auto &robot = store.getRobot();
  auto &uart = store.getUart();
  using namespace config::pid::traslation;
  auto pid = PID(Kp, Ki, Kd);

  while (true) {
    xTaskNotifyWait(0x00,    /* Don't clear any notification bits on entry. */
                    0,       /* Reset the notification value to 0 on exit. */
                    nullptr, /* Notified value pass out in
                                         ulNotifiedValue. */
                    portMAX_DELAY); /* Block indefinitely. */

    uart.writeline("Traslation Task Called!");
    // uart.writeline("Traslation pid: " +
    //                std::to_string(pid(store.getPositionDiff())));

    // Traslation PID
    auto action = pid(store.getPositionDiff());
    robot.move(action);
  };
}
static void prvRotationControlTask(void *pvParameters) {

  auto &store = Store::instance();
  auto &uart = store.getUart();
  auto &robot = store.getRobot();

  using namespace config::pid::rotation;
  auto pid = PID(Kp, Ki, Kd);

  while (true) {
    xTaskNotifyWait(0x00,    /* Don't clear any notification bits on entry. */
                    0,       /* Reset the notification value to 0 on exit. */
                    nullptr, /* Notified value pass out in
                                         ulNotifiedValue. */
                    portMAX_DELAY); /* Block indefinitely. */

    uart.writeline("Rotation Task Called!");
    // Rotation PID
    auto action = pid(store.getRotationDiff());
    // Robot Control
    robot.rotate(action);
  };
}
static void prvCalibrateSensorsTask(void *pvParameters) {

  auto &store = Store::instance();
  auto &uart = store.getUart();

  while (true) {
    xTaskNotifyWait(0x00,    /* Don't clear any notification bits on entry. */
                    0,       /* Reset the notification value to 0 on exit. */
                    nullptr, /* Notified value pass out in
                                         ulNotifiedValue. */
                    portMAX_DELAY); /* Block indefinitely. */

    uart.writeline("Calibration Task Called!");
    // Calibration Routine
  };
}
static void prvProcessIncomingMessageTask(void *pvParameters) {

  auto &store = Store::instance();
  auto &uart = store.getUart();
  auto parser = MessageParser();

  while (true) {
    xTaskNotifyWait(0x00,    /* Don't clear any notification bits on entry. */
                    0,       /* Reset the notification value to 0 on exit. */
                    nullptr, /* Notified value pass out in
                                         ulNotifiedValue. */
                    portMAX_DELAY); /* Block indefinitely. */

    uart.writeline("Process Incoming Message Task!");
    if (uart.hasMessages()) {
      uart.writeline("Available messages: " +
                     std::to_string(uart.hasMessages()));
      auto message = uart.getMessage();
      uart.writeline(message);
      auto newTarget = parser.parse(message);
      if (newTarget.first != 0 || newTarget.second != 0)
        store.updateTargets(newTarget);
    }
  };
}
static void prvReportMovementTask(void *pvParameters) {

  auto &store = Store::instance();
  auto &uart = store.getUart();
  auto parser = MessageParser();
  std::pair<float, float> payload;

  while (true) {
    // xTaskNotifyWait(0x00,    /* Don't clear any notification bits on entry.
    // */
    //                 0,       /* Reset the notification value to 0 on exit.
    //                 */ nullptr, /* Notified value pass out in
    //                                      ulNotifiedValue. */
    //                 portMAX_DELAY); /* Block indefinitely. */
    if (xQueueReceive(xDeltaReportsQueue, &payload, portMAX_DELAY) == pdPASS) {
      uart.writeline("Report Movement Task Called!");
      auto message = parser.encode(payload);
      uart.writeline(message);
    }
  };
}
