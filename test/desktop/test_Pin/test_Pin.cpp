#include "proxies/gpioProxy.hpp"
#include "proxies/irqProxy.hpp"
#include <Pin.hpp>

#include "testing/fff.h"
#include "gtest/gtest.h"

FAKE_VOID_FUNC(gpio_init, uint);
FAKE_VOID_FUNC(gpio_set_dir, uint, bool);
FAKE_VOID_FUNC(gpio_put, uint, bool);
FAKE_VOID_FUNC(gpio_pull_up, uint);
FAKE_VOID_FUNC(gpio_pull_down, uint);
FAKE_VOID_FUNC(gpio_set_function, uint, gpio_function);
FAKE_VALUE_FUNC(bool, gpio_get, uint);
FAKE_VOID_FUNC(gpio_set_irq_enabled, uint, uint32_t, bool);
FAKE_VOID_FUNC(gpio_set_irq_enabled_with_callback, uint, uint32_t, bool,
               gpio_irq_callback_t);

// irq handler fakes
FAKE_VOID_FUNC(irq_set_enabled, uint, bool);
FAKE_VOID_FUNC(irq_set_exclusive_handler, uint, irqHandler);

class PinTest : public ::testing::Test {
protected:
  void SetUp() {
    RESET_FAKE(gpio_init);
    RESET_FAKE(gpio_set_dir);
    RESET_FAKE(gpio_put);
    RESET_FAKE(gpio_pull_up);
    RESET_FAKE(gpio_pull_down);
    RESET_FAKE(gpio_get);
    RESET_FAKE(gpio_set_function);
    RESET_FAKE(gpio_set_irq_enabled);
    RESET_FAKE(gpio_set_irq_enabled_with_callback);
    RESET_FAKE(gpio_set_function);

    RESET_FAKE(irq_set_enabled);
    RESET_FAKE(irq_set_exclusive_handler);
    FFF_RESET_HISTORY();
  };
  const int SHARED_PIN = 7;
  Pin sharedPin{SHARED_PIN};
};

TEST_F(PinTest, ShouldBuild) {
  const uint PIN = 10;
  Pin pin{PIN};
  EXPECT_EQ(gpio_init_fake.call_count, 1);
  EXPECT_EQ(gpio_init_fake.arg0_val, PIN);

  EXPECT_EQ(gpio_set_dir_fake.call_count, 1);
  EXPECT_EQ(gpio_set_dir_fake.arg0_val, PIN);
  EXPECT_EQ(gpio_set_dir_fake.arg1_val, 1);

  EXPECT_EQ(gpio_pull_down_fake.call_count, 0);
  EXPECT_EQ(gpio_pull_up_fake.call_count, 0);

  EXPECT_EQ(gpio_put_fake.call_count, 1);
  EXPECT_EQ(gpio_put_fake.arg0_val, PIN);
  EXPECT_EQ(gpio_put_fake.arg1_val, 0);
};

TEST_F(PinTest, SetsValue) {
  sharedPin.value(true);
  EXPECT_EQ(gpio_put_fake.call_count, 1);
  EXPECT_EQ(gpio_put_fake.arg1_val, true);
  sharedPin.value(false);
  EXPECT_EQ(gpio_put_fake.call_count, 2);
  EXPECT_EQ(gpio_put_fake.arg1_val, false);
}

TEST_F(PinTest, GetsValue) {
  gpio_get_fake.return_val = false;
  EXPECT_EQ(sharedPin.value(), false);
  gpio_get_fake.return_val = true;
  EXPECT_EQ(sharedPin.value(), true);
}

TEST_F(PinTest, TogglesPin) {
  gpio_get_fake.return_val = true;
  sharedPin.toggle();
  EXPECT_EQ(gpio_put_fake.call_count, 1);
  EXPECT_EQ(gpio_put_fake.arg1_val, false);
}

TEST_F(PinTest, TurnsPinOn) {
  sharedPin.on();
  EXPECT_EQ(gpio_put_fake.call_count, 1);
  EXPECT_EQ(gpio_put_fake.arg1_val, true);
}
TEST_F(PinTest, TurnsPinOff) {
  sharedPin.off();
  EXPECT_EQ(gpio_put_fake.call_count, 1);
  EXPECT_EQ(gpio_put_fake.arg1_val, false);
}
