#include "UART.hpp"
#include "mocks/PinMock.hpp"
#include "proxies/gpioProxy.hpp"
#include "proxies/irqProxy.hpp"
#include "testing/fff.h"
#include "gtest/gtest.h"

FAKE_VOID_FUNC(uart_init, uint *, uint);
FAKE_VOID_FUNC(uart_deinit, uint *);
FAKE_VOID_FUNC(uart_write_blocking, uart_inst_t *, const uint8_t *, size_t);
FAKE_VOID_FUNC(uart_read_blocking, uart_inst_t *, uint8_t *, size_t);
FAKE_VOID_FUNC(uart_puts, uart_inst_t *, const char *);
FAKE_VALUE_FUNC(uint, uart_is_readable, uart_inst_t *);
FAKE_VALUE_FUNC(char, uart_getc, uart_inst_t *);
FAKE_VOID_FUNC(uart_set_fifo_enabled, uart_inst_t *, bool);
FAKE_VOID_FUNC(uart_set_irq_enables, uart_inst_t *, bool, bool);

FAKE_VOID_FUNC(gpio_set_irq_enabled, uint, uint32_t, bool);
FAKE_VOID_FUNC(gpio_set_irq_enabled_with_callback, uint, uint32_t, bool,
               gpio_irq_callback_t);

FAKE_VOID_FUNC(irq_set_enabled, uint, bool);
FAKE_VOID_FUNC(irq_set_exclusive_handler, uint, irqHandler);

using ::testing::AtLeast;
using ::testing::NiceMock;

class UARTTest : public ::testing::Test {
protected:
  void SetUp() {
    RESET_FAKE(uart_init);
    RESET_FAKE(uart_deinit);
    RESET_FAKE(uart_write_blocking);
    RESET_FAKE(uart_read_blocking);
    RESET_FAKE(uart_puts);
    RESET_FAKE(uart_is_readable);
    RESET_FAKE(uart_getc);
    RESET_FAKE(uart_set_fifo_enabled);
    RESET_FAKE(uart_set_irq_enables);

    RESET_FAKE(gpio_set_irq_enabled);
    RESET_FAKE(gpio_set_irq_enabled_with_callback);

    RESET_FAKE(irq_set_enabled);
    RESET_FAKE(irq_set_exclusive_handler);
    FFF_RESET_HISTORY();
  };
};

TEST_F(UARTTest, ShouldBuild) {
  auto tx = std::make_unique<NiceMock<PinMock>>();
  auto rx = std::make_unique<NiceMock<PinMock>>();
  auto uart =
      UART::Builder().withRX(std::move(rx)).withTX(std::move(tx)).build();
  EXPECT_EQ(*uart_init_fake.arg0_val, 0);
  EXPECT_EQ(uart_init_fake.arg1_val, 115200);
};
