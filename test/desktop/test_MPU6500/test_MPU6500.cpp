#include "mocks/I2CMock.hpp"
#include "gtest/gtest.h"
#include <MPU6500.hpp>
#include <memory>

// using namespace IMU::MPU6500;
using ::testing::NiceMock;
using ::testing::Return;

class MPU6500Test : public ::testing::TestWithParam<std::vector<uint8_t>> {
protected:
  std::unique_ptr<NiceMock<I2CMock>> MockI2C =
      std::make_unique<NiceMock<I2CMock>>();

  std::function<MPU6500(void)> build = [&]() {
    return MPU6500(std::move(MockI2C));
  };
};

TEST_F(MPU6500Test, BuildsMPU6500) {

  using vec = std::vector<uint8_t>;

  EXPECT_CALL(*MockI2C, setAddress(0x68));
  EXPECT_CALL(*MockI2C, write(vec{107U, 0})).WillOnce(Return(true));
  EXPECT_CALL(*MockI2C, write(vec{107U, 1})).WillOnce(Return(true));
  EXPECT_CALL(*MockI2C, write(vec{26U, 0})).WillOnce(Return(true));
  EXPECT_CALL(*MockI2C, write(vec{27U, 0})).WillOnce(Return(true));
  EXPECT_CALL(*MockI2C, write(vec{28U, 0, 8})).WillOnce(Return(true));

  auto mpu = build();
};

TEST_P(MPU6500Test, ReadAccelerometerX) {
  auto rawVector = GetParam();
  EXPECT_CALL(*MockI2C, readWord(0x3B, 2))
      .WillOnce(Return((rawVector.at(0) << 8 | rawVector.at(1))));
  auto expectedValue =
      ((rawVector.at(0) << 8) | rawVector.at(1)) / 32768.0 * 2.0;

  auto mpu = build();
  EXPECT_NEAR(mpu.getAccelerationX(), expectedValue, 0.0001);
};

TEST_P(MPU6500Test, ReadAccelerometerY) {
  auto rawVector = GetParam();
  EXPECT_CALL(*MockI2C, readWord(0x3B + 2, 2))
      .WillOnce(Return((rawVector.at(0) << 8 | rawVector.at(1))));
  auto expectedValue =
      ((rawVector.at(0) << 8) | rawVector.at(1)) / 32768.0 * 2.0;

  auto mpu = build();
  EXPECT_NEAR(mpu.getAccelerationY(), expectedValue, 0.0001);
};

TEST_P(MPU6500Test, ReadAccelerometerZ) {
  auto rawVector = GetParam();
  EXPECT_CALL(*MockI2C, readWord(0x3B + 4, 2))
      .WillOnce(Return((rawVector.at(0) << 8 | rawVector.at(1))));
  auto expectedValue =
      ((rawVector.at(0) << 8 | rawVector.at(1))) / 32768.0 * 2.0;

  auto mpu = build();
  EXPECT_NEAR(mpu.getAccelerationZ(), expectedValue, 0.0001);
};

TEST_P(MPU6500Test, ReadGyroscopeX) {
  auto rawVector = GetParam();
  EXPECT_CALL(*MockI2C, readWord(0x43, 2))
      .WillOnce(Return((rawVector.at(0) << 8 | rawVector.at(1))));
  auto expectedValue =
      ((rawVector.at(0) << 8 | rawVector.at(1))) / 32768.0 * 250.0;
  auto mpu = build();
  EXPECT_NEAR(mpu.getGyroX(), expectedValue, 0.0001);
};

TEST_P(MPU6500Test, ReadGyroscopeY) {
  auto rawVector = GetParam();
  EXPECT_CALL(*MockI2C, readWord(0x43 + 2, 2))
      .WillOnce(Return((rawVector.at(0) << 8 | rawVector.at(1))));
  auto expectedValue =
      ((rawVector.at(0) << 8 | rawVector.at(1))) / 32768.0 * 250.0;
  auto mpu = build();
  EXPECT_NEAR(mpu.getGyroY(), expectedValue, 0.0001);
};

TEST_P(MPU6500Test, ReadGyroscopeZ) {
  auto rawVector = GetParam();
  EXPECT_CALL(*MockI2C, readWord(0x43 + 4, 2))
      .WillOnce(Return((rawVector.at(0) << 8 | rawVector.at(1))));
  auto expectedValue =
      ((rawVector.at(0) << 8 | rawVector.at(1))) / 32768.0 * 250.0;

  auto mpu = build();
  EXPECT_NEAR(mpu.getGyroZ(), expectedValue, 0.0001);
};

INSTANTIATE_TEST_SUITE_P(Lectures, MPU6500Test,
                         ::testing::Values(std::vector<uint8_t>{0x80, 0x00},
                                           std::vector<uint8_t>{0xF0, 0x56},
                                           std::vector<uint8_t>{0x00, 0x00},
                                           std::vector<uint8_t>{0x34, 0x56},
                                           std::vector<uint8_t>{0x7F, 0xFF}));
