#include <MPU6500.hpp>
#include <array>
#include <cmath>
#include <stdlib.h>

constexpr uint8_t MPU6500_ADDRESS = 0x68;
constexpr uint8_t MPU6500_R_XG_OFFSET_H = 0x13; // 0d19
constexpr uint8_t MPU6500_R_XG_OFFSET_L = 0x14; // 0d20
constexpr uint8_t MPU6500_R_YG_OFFSET_H = 0x15; // 0d21
constexpr uint8_t MPU6500_R_YG_OFFSET_L = 0x16; // 0d22
constexpr uint8_t MPU6500_R_ZG_OFFSET_H = 0x17; // 0d23
constexpr uint8_t MPU6500_R_ZG_OFFSET_L = 0x18; // 0d24
constexpr uint8_t MPU6500_R_CONFIG = 0x1A;      // 0d26
constexpr uint8_t MPU6500_R_GYRO_CONF = 0x1B;   // 0d27
constexpr uint8_t MPU6500_R_ACC_CONF = 0x1C;    // 0d28
constexpr uint8_t MPU6500_R_ACC_CONF2 = 0x1D;   // 0d29
constexpr uint8_t MPU6500_R_ACC_XOUT_H = 0x3B;  // 0d59
constexpr uint8_t MPU6500_R_ACC_XOUT_L = 0x3C;  // 0d60
constexpr uint8_t MPU6500_R_ACC_YOUT_H = 0x3D;  // 0d61
constexpr uint8_t MPU6500_R_ACC_YOUT_L = 0x3E;  // 0d62
constexpr uint8_t MPU6500_R_ACC_ZOUT_H = 0x3F;  // 0d63
constexpr uint8_t MPU6500_R_ACC_ZOUT_L = 0x40;  // 0d64
constexpr uint8_t MPU6500_R_TEMP_OUT_H = 0x41;  // 0d65
constexpr uint8_t MPU6500_R_TEMP_OUT_L = 0x42;  // 0d66
constexpr uint8_t MPU6500_R_GYRO_XOUT_H = 0x43; // 0d67
constexpr uint8_t MPU6500_R_GYRO_XOUT_L = 0x44; // 0d68
constexpr uint8_t MPU6500_R_GYRO_YOUT_H = 0x45; // 0d69
constexpr uint8_t MPU6500_R_GYRO_YOUT_L = 0x46; // 0d70
constexpr uint8_t MPU6500_R_GYRO_ZOUT_H = 0x47; // 0d71
constexpr uint8_t MPU6500_R_GYRO_ZOUT_L = 0x48; // 0d82
constexpr uint8_t MPU6500_R_PWR_MGMT_1 = 0x6B;  // 0d107
constexpr uint8_t MPU6500_R_WHO_AM_I = 0x75;    // 0d117
constexpr uint8_t MPU6500_R_XA_OFFSET_H = 0x77; // 0d119
constexpr uint8_t MPU6500_R_XA_OFFSET_L = 0x78; // 0d120
constexpr uint8_t MPU6500_R_YA_OFFSET_H = 0x7A; // 0d122
constexpr uint8_t MPU6500_R_YA_OFFSET_L = 0x7B; // 0d122
constexpr uint8_t MPU6500_R_ZA_OFFSET_H = 0x7D; // 0d125
constexpr uint8_t MPU6500_R_ZA_OFFSET_L = 0x7E; // 0d126

// using namespace IMU::MPU6500;

// using std::vector;

constexpr std::array<float, 4> accelScale = {2.0, 4.0, 8.0, 16.0};
constexpr std::array<uint8_t, 4> accelConfBits = {0x00, 0x08, 0x10, 0x18};
constexpr std::array<float, 4> gyroScale = {250.0, 500.0, 1000.0, 2000.0};
constexpr std::array<uint8_t, 4> gyroConfBits = {0x00, 0x08, 0x10, 0x18};

MPU6500::MPU6500(std::unique_ptr<II2C> i2c, GyroScale gyroScale,
                 AccelScale accScale)
    : i2c(std::move(i2c)) {
  MPUconfig.gyroScale = gyroScale;
  MPUconfig.accelScale = accScale;
  init();
};

MPU6500::MPU6500(Config config)
    : MPU6500(std::move(config.i2c), config.gyroScale, config.accelScale){};

MPU6500::~MPU6500(){};

void MPU6500::init() const {
  this->i2c->setAddress(0x68);
  // this->i2c->write({0x6B, 0x00});

  this->i2c->write({MPU6500_R_PWR_MGMT_1, 0b00000000});
  this->i2c->write({MPU6500_R_PWR_MGMT_1, 0b00000001});
  this->i2c->write({MPU6500_R_CONFIG, 0b00000000});
  this->i2c->write({MPU6500_R_GYRO_CONF, gyroConfBits.at(0)});
  this->i2c->write({MPU6500_R_ACC_CONF, accelConfBits.at(0), 0b00001000});
};

bool MPU6500::isConnected() const {
  return (i2c->read(1, MPU6500_R_WHO_AM_I).front() == 0x70);
};

float MPU6500::getAcceleration(uint8_t address) const {
  const auto raw = i2c->readWord(address, 2);
  return (raw * 2.0) / 32768.0;
}
float MPU6500::getGyro(uint8_t address) const {
  const auto raw = i2c->readWord(address, 2);
  return (raw * 250.0) / 32768.0;
}

float MPU6500::getAccelerationX() const {
  auto val = getAcceleration(0x3B) - accXOffset;
  if (abs(val) > accXThreshold) {
    return val;
  } else {
    return 0;
  }
};
float MPU6500::getAccelerationY() const {
  auto val = getAcceleration(0x3B + 2) - accXOffset;
  if (abs(val) > accYThreshold) {
    return val;
  } else {
    return 0;
  }
};
float MPU6500::getAccelerationZ() const {
  auto val = getAcceleration(0x3B + 4) - accXOffset;
  if (abs(val) > accZThreshold) {
    return val;
  } else {
    return 0;
  }
};
float MPU6500::getGyroX() const {
  auto val = getGyro(0x43) - gyroXOffset;
  if (abs(val) > gyroXThreshold) {
    return val;
  } else {
    return 0;
  }
}
float MPU6500::getGyroY() const {
  auto val = getGyro(0x43 + 2) - gyroYOffset;
  if (abs(val) > gyroYThreshold) {
    return val;
  } else {
    return 0;
  }
}
float MPU6500::getGyroZ() const {
  auto val = getGyro(0x43 + 4) - gyroZOffset;
  if (abs(val) > gyroZThreshold) {
    return val;
  } else {
    return 0;
  }
}

void MPU6500::setGyroXOffset(float newOffset) {
  gyroXOffset += newOffset;
  float aux = newOffset * 32768.0 / 250.0;
  i2c->writeWord(MPU6500_R_XG_OFFSET_H, static_cast<int16_t>(aux));
};
void MPU6500::setGyroYOffset(float newOffset) {
  gyroYOffset += newOffset;
  float aux = newOffset * 32768.0 / 250.0;
  i2c->writeWord(MPU6500_R_YG_OFFSET_H, static_cast<int16_t>(aux));
};
void MPU6500::setGyroZOffset(float newOffset) {
  gyroZOffset += newOffset;
  float aux = newOffset * 32768.0 / 250.0;
  i2c->writeWord(MPU6500_R_ZG_OFFSET_H, static_cast<int16_t>(aux));
};
void MPU6500::setAccXOffset(float newOffset) {
  accXOffset += newOffset;
  float aux = newOffset * 32768.0 / 250.0;
  i2c->writeWord(MPU6500_R_XA_OFFSET_H, static_cast<int16_t>(aux));
};
void MPU6500::setAccYOffset(float newOffset) {
  accYOffset += newOffset;
  float aux = newOffset * 32768.0 / 250.0;
  i2c->writeWord(MPU6500_R_YA_OFFSET_H, static_cast<int16_t>(aux));
};
void MPU6500::setAccZOffset(float newOffset) {
  accZOffset += newOffset;
  float aux = newOffset * 32768.0 / 250.0;
  i2c->writeWord(MPU6500_R_ZA_OFFSET_H, static_cast<int16_t>(aux));
};

void MPU6500::calibrateXGyro() {
  float acum = 0.0;
  for (uint8_t i = 0; i < iLimit; i++) {
    acum += getGyroX();
  }
  setGyroXOffset(acum / iLimit);
};
void MPU6500::calibrateYGyro() {
  float acum = 0.0;
  for (uint8_t i = 0; i < iLimit; i++) {
    acum += getGyroY();
  }
  setGyroYOffset(acum / iLimit);
};
void MPU6500::calibrateZGyro() {
  float acum = 0.0;
  for (uint8_t i = 0; i < iLimit; i++) {
    acum += getGyroZ();
  }
  setGyroZOffset(acum / iLimit);
};

void MPU6500::calibrateXAcc() {
  float acumOffset = 0.0;
  std::vector<float> values;
  for (uint8_t i = 0; i < iLimit; i++) {
    auto current = getAccelerationX();
    values.push_back(current);
    acumOffset += current;
  }
  setAccXOffset(acumOffset / iLimit);
  float acumThreshold = 0.0;
  for (uint8_t i = 0; i < iLimit; i++) {
    acumThreshold += (values.at(i) - accXOffset) * (values.at(i) - accXOffset);
  }
  accXThreshold = sqrt(acumThreshold / iLimit);
};
void MPU6500::calibrateYAcc() {
  float acumOffset = 0.0;
  std::vector<float> values;
  for (uint8_t i = 0; i < iLimit; i++) {
    auto current = getAccelerationY();
    values.push_back(current);
    acumOffset += current;
  }
  setAccYOffset(acumOffset / iLimit);
  float acumThreshold = 0.0;
  for (uint8_t i = 0; i < iLimit; i++) {
    acumThreshold += (values.at(i) - accYOffset) * (values.at(i) - accYOffset);
  }
  accYThreshold = sqrt(acumThreshold / iLimit);
};
void MPU6500::calibrateZAcc() {
  float acumOffset = 0.0;
  std::vector<float> values;
  for (uint8_t i = 0; i < iLimit; i++) {
    auto current = getAccelerationZ();
    values.push_back(current);
    acumOffset += current;
  }
  setAccZOffset(acumOffset / iLimit);
  float acumThreshold = 0.0;
  for (uint8_t i = 0; i < iLimit; i++) {
    acumThreshold += (values.at(i) - accZOffset) * (values.at(i) - accZOffset);
  }
  accZThreshold = sqrt(acumThreshold / iLimit);
};

void MPU6500::calibrateGyroAxis(uint8_t axis) {
  if (axis & Axis::X) {
    calibrateXGyro();
  }
  if (axis & Axis::Y) {
    calibrateYGyro();
  }
  if (axis & Axis::Z) {
    calibrateZGyro();
  }
};
void MPU6500::calibrateAccAxis(uint8_t axis) {
  if (axis & Axis::X) {
    calibrateXAcc();
  }
  if (axis & Axis::Y) {
    calibrateYAcc();
  }
  if (axis & Axis::Z) {
    calibrateZAcc();
  }
};