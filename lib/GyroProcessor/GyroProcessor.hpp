#pragma once

#include "interfaces/IFilter.hpp"
#include "interfaces/IIntegrator.hpp"
#include "interfaces/IMU/IGyroProcessor.hpp"
#include <LeakyFilter.hpp>
#include <MedianFilter.hpp>
#include <SimpsonIntegrator.hpp>
#include <interfaces/IBuilder.hpp>
#include <memory>

class GyroProcessor : public IGyroProcessor {
private:
  std::unique_ptr<IFilter> firstFilter;
  std::unique_ptr<IFilter> secondFilter;
  std::unique_ptr<IIntegrator> integrator;

public:
  typedef struct {
    std::unique_ptr<IFilter> firstFilter = nullptr;
    std::unique_ptr<IFilter> secondFilter = nullptr;
    std::unique_ptr<IIntegrator> integrator = nullptr;
  } Config;
  class Builder;
  GyroProcessor(std::unique_ptr<IFilter> firstFilter,
                std::unique_ptr<IFilter> secondFilter,
                std::unique_ptr<IIntegrator> integrator);
  explicit GyroProcessor(Config config);
  ~GyroProcessor();
  float getAngularSpeed(float rawValue) override;
  float getAngle(std::pair<float, float> rawMeasure) override;
};

class GyroProcessor::Builder : IBuilder<GyroProcessor> {
private:
  GyroProcessor::Config config;

public:
  Builder &withFirstFilter(std::unique_ptr<IFilter> firstFilter) {
    this->config.firstFilter = std::move(firstFilter);
    return *this;
  };
  Builder &withFirstFilter(float lambda) {
    this->config.firstFilter = std::make_unique<LeakyFilter>(lambda);
    return *this;
  };
  Builder &withSecondFilter(std::unique_ptr<IFilter> secondFilter) {
    this->config.secondFilter = std::move(secondFilter);
    return *this;
  };
  Builder &withSecondFilter(uint8_t qSize) {
    this->config.secondFilter = std::make_unique<MedianFilter>(qSize);
    return *this;
  };
  Builder &withIntegrator(std::unique_ptr<IIntegrator> integrator) {
    this->config.integrator = std::move(integrator);
    return *this;
  };

  GyroProcessor build() {
    if (this->config.firstFilter == nullptr) {
      this->config.firstFilter = std::make_unique<LeakyFilter>(0.2);
    }
    if (this->config.secondFilter == nullptr) {
      this->config.secondFilter = std::make_unique<MedianFilter>(5);
    }
    if (this->config.integrator == nullptr) {
      this->config.integrator = std::make_unique<SimpsonIntegrator>();
    }
    return GyroProcessor{std::move(this->config)};
  };

  std::unique_ptr<GyroProcessor> get() {
    if (this->config.firstFilter == nullptr) {
      this->config.firstFilter = std::make_unique<LeakyFilter>(0.2);
    }
    if (this->config.secondFilter == nullptr) {
      this->config.secondFilter = std::make_unique<MedianFilter>(5);
    }
    if (this->config.integrator == nullptr) {
      this->config.integrator = std::make_unique<SimpsonIntegrator>();
    }
    return std::move(std::make_unique<GyroProcessor>(std::move(this->config)));
  };
};