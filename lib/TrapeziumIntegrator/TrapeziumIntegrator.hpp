#pragma once

#include "interfaces/IIntegrator.hpp"

class TrapeziumIntegrator : public IIntegrator {
private:
  float delta;
  float lastValue;

public:
  TrapeziumIntegrator();
  ~TrapeziumIntegrator();
  float integrate(float newValue, float newDt);
  void addValue(float newValue);
};