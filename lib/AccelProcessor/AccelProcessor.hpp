#pragma once

#include "interfaces/IFilter.hpp"
#include "interfaces/IIntegrator.hpp"
#include "interfaces/IMU/IAccelProcessor.hpp"
#include <memory>

class AccelProcessor : public IAccelProcessor {
private:
  std::unique_ptr<IFilter> aFil;
  std::unique_ptr<IFilter> bFil;
  std::unique_ptr<IIntegrator> aInteg;
  // std::unique_ptr<IIntegrator> bInteg;

public:
  AccelProcessor(std::unique_ptr<IFilter> aFil, std::unique_ptr<IFilter> bFil,
                 std::unique_ptr<IIntegrator> aInteg);
  //  std::unique_ptr<IIntegrator> bInteg);
  ~AccelProcessor();
  float getAcceleration(float rawValue) override;
  float getLinearSpeed(std::pair<float, float> rawMeasure) override;
  // float getPosition(std::pair<float, float> rawMeasure) override;
};