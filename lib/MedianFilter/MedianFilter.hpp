#pragma once

#include "interfaces/IFilter.hpp"
#include <algorithm>
#include <deque>

class MedianFilter : public IFilter {
private:
  std::deque<float> values;

public:
  explicit MedianFilter(uint8_t newQSize);
  ~MedianFilter();
  float filter(float rawData) override;
};