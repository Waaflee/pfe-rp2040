#include <MedianFilter.hpp>

MedianFilter::MedianFilter(uint8_t newQSize) { values.resize(newQSize, 0.0); };

MedianFilter::~MedianFilter(){};

float MedianFilter::filter(float newValue) {
  values.push_back(newValue);
  values.pop_front();

  auto sortedValues = values;
  std::sort(sortedValues.begin(), sortedValues.end());

  return sortedValues.at(values.size() / 2);
};
