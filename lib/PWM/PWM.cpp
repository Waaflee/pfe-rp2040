#include "proxies/pwmProxy.hpp"
#include <PWM.hpp>
#include <math.h>

PWM::PWM(std::unique_ptr<IPin> pin, const uint frequency, const uint duty) {
  this->pin = std::move(pin);
  this->slice = pwm_gpio_to_slice_num(this->pin->getPin());
  this->channel = pwm_gpio_to_channel(this->pin->getPin());
  this->frequency(frequency);
  this->duty(duty);
  pwm_set_enabled(this->slice, true);
};

PWM::PWM(Config config)
    : PWM(std::move(config.pin), config.frequency, config.duty){};

PWM::~PWM() { pwm_set_enabled(slice, false); };

uint PWM::frequency() const { return std::round(clock / wrap); };
void PWM::frequency(const uint frequency) {
  uint divider16 = clock / frequency / 4096 + (clock % (frequency * 4096) != 0);
  divider16 = divider16 / 16 == 0 ? 16 : divider16;

  uint wrap = clock * 16 / divider16 / frequency - 1;

  pwm_set_clkdiv_int_frac(this->slice, divider16 / 16, divider16 & 0xF);
  pwm_set_wrap(this->slice, wrap);

  this->wrap = wrap;
};
uint PWM::duty() const { return std::round(level * 100 / wrap); };
void PWM::duty(const uint duty) {
  auto const clipped_duty = duty >= 100 ? 100 : duty;
  this->level = wrap * clipped_duty / 100;
  pwm_set_chan_level(slice, channel, this->level);
};
void PWM::deinit() const { pwm_set_enabled(this->slice, false); }