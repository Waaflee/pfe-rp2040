#pragma once

#include "interfaces/IFilter.hpp"

class LeakyFilter : public IFilter {
public:
  explicit LeakyFilter(float builderLambda);
  ~LeakyFilter();
  float filter(float currentMeasure) override;
  void setLambda(float newLambda);

private:
  float pastValue;
  float lambda;
};