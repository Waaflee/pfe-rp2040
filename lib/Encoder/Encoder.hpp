#pragma once
#include "interfaces/IBuilder.hpp"
#include "interfaces/Motors/Sensors/IEncoder.hpp"
#include <Pin.hpp>
#include <functional>
#include <memory>

class Encoder : public IEncoder {
private:
  std::unique_ptr<IPin> input;
  float step = 0;
  uint event = IPin::IRQTrigger::FALLING;
  std::unique_ptr<IPin> directionPin = nullptr;
  bool direction = true;
  std::function<void(void)> callback;

  int previousSteps = 0;
  int steps = 0;

  static uint InstanceCounter;
  uint InstanceIndex;

  void update();

public:
  typedef struct {
    std::unique_ptr<IPin> input;
    float step = 18.0f;
    uint event = IPin::FALLING;
    std::unique_ptr<IPin> directionPin = nullptr;
    bool direction = true;
    std::function<void(void)> callback = nullptr;
  } Config;
  class Builder;
  explicit Encoder(std::unique_ptr<IPin> input, float step = 18.0f,
                   uint event = IPin::FALLING,
                   std::unique_ptr<IPin> directionPin = nullptr,
                   bool direction = true,
                   std::function<void(void)> fn = nullptr);
  explicit Encoder(Config config);
  ~Encoder();

  void setSteps(int steps) override { this->steps = steps; };
  uint getSteps() override { return this->steps; };
  int getDeltaSteps() override {
    auto delta = steps - previousSteps;
    previousSteps = steps;
    return delta;
  };

  float getAngle() override { return getSteps() * step; };
  float getDeltaAngle() override { return getDeltaSteps() * step; };
  //   float getSpeed();
  //   float getDeltaSpeed();
  //   float getAcceleration();
  //   float getDeltaAcceleration();

  void setDirection(bool direction) override { this->direction = direction; };
  bool getDirection(bool direction) override { return this->direction; };

  void reset() {
    this->steps = 0;
    this->previousSteps = 0;
  };
};

class Encoder::Builder : IBuilder<Encoder> {
private:
  Encoder::Config config;

public:
  Builder &at(uint pin) {
    this->config.input =
        std::move(std::make_unique<Pin>(pin, IPin::Direction::IN));
    return *this;
  }
  Builder &at(std::unique_ptr<Pin> pin) {
    this->config.input = std::move(pin);
    return *this;
  }
  Builder &withDirectionAt(uint pin) {
    this->config.directionPin =
        std::move(std::make_unique<Pin>(pin, IPin::Direction::IN));
    return *this;
  }
  Builder &withDirection(std::unique_ptr<Pin> pin) {
    this->config.directionPin = std::move(pin);
    return *this;
  }
  Builder &withStep(float step) {
    this->config.step = step;
    return *this;
  }
  Builder &onEvents(uint events) {
    this->config.event = events;
    return *this;
  }
  Builder &withCallback(std::function<void(void)> fn) {
    this->config.callback = fn;
    return *this;
  }

  Encoder build() { return Encoder{std::move(config)}; };

  std::unique_ptr<Encoder> get() {
    return std::move(std::make_unique<Encoder>(std::move(config)));
  };
};
