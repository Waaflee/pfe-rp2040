#pragma once
#include "interfaces/Communications/II2C.hpp"
#include "interfaces/IBuilder.hpp"
#include <Pin.hpp>
#include <memory>

class I2C : public II2C {
private:
  i2c_inst_t *instance;
  uint8_t address = 0x00;
  std::unique_ptr<IPin> SDA;
  std::unique_ptr<IPin> SCL;
  bool isReservedAddress(uint8_t address) const;
  bool writeWord(uint8_t registry, int word, uint8_t size) const;

public:
  typedef struct {
    std::unique_ptr<IPin> SDA;
    std::unique_ptr<IPin> SCL;
    uint frequency = 400 * 1000;
    II2C::I2CInstance instance = I2C0;
  } Config;
  class Builder;
  explicit I2C(std::unique_ptr<IPin> SDA, std::unique_ptr<IPin> SCL,
               const uint frequency = 400 * 1000,
               II2C::I2CInstance instance = I2C0);
  explicit I2C(Config config);
  ~I2C();
  std::vector<uint8_t> scan() const override;
  bool write(std::vector<uint8_t> data) const override;
  std::vector<uint8_t> read(uint8_t bytes, uint8_t registry) const override;
  bool writeTo(std::vector<uint8_t> data, uint8_t address) const override;
  std::vector<uint8_t> readFrom(uint8_t bytes, uint8_t registry,
                                uint8_t address) const override;
  void deinit() const override;
  void setAddress(uint8_t address) override;
  int readWord(uint8_t registry, uint8_t size = 2) const override;

  bool writeWord(uint8_t registry, int16_t data) const override;
  bool writeWord(uint8_t registry, int32_t data) const override;
};

class I2C::Builder : IBuilder<I2C> {
private:
  I2C::Config config;

public:
  Builder &withSDA(uint sda) {
    this->config.SDA = std::move(
        std::make_unique<Pin>(sda, IPin::Direction::NO_DIRECTION,
                              IPin::Pull::NO_PULL, IPin::Function::I2C));
    return *this;
  };

  Builder &withSCL(uint scl) {
    this->config.SCL = std::move(
        std::make_unique<Pin>(scl, IPin::Direction::NO_DIRECTION,
                              IPin::Pull::NO_PULL, IPin::Function::I2C));
    return *this;
  };

  Builder &withFrequency(uint frequency) {
    this->config.frequency = frequency;
    return *this;
  };

  Builder &withInstance(II2C::I2CInstance instance) {
    this->config.instance = instance;
    return *this;
  };

  I2C build() override { return I2C{std::move(config)}; };

  std::unique_ptr<I2C> get() override {
    return std::move(std::make_unique<I2C>(std::move(config)));
  };
};