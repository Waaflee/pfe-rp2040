#pragma once
#include "interfaces/IPWM.hpp"
#include "interfaces/IPin.hpp"
#include <gmock/gmock.h>

class PWMMock : public IPWM {
public:
  MOCK_METHOD(uint, frequency, (), (const, override));
  MOCK_METHOD(void, frequency, (const uint frequency), (override));
  MOCK_METHOD(uint, duty, (), (const, override));
  MOCK_METHOD(void, duty, (const uint frequency), (override));
  MOCK_METHOD(void, deinit, (), (const, override));
};
