#pragma once
#include "interfaces/Filters/IFilter.hpp"
#include <gmock/gmock.h>

template <typename Input, typename Output>
class FilterMock : public IFilter<Input, Output> {
public:
  MOCK_METHOD(Output, filter, (Input data), (override));
};