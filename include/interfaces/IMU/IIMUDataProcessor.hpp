#pragma once

#include <interfaces/NonCopyable.hpp>

class IIMUDataProcessor : NonCopyable {
public:
  virtual ~IIMUDataProcessor(){};
  virtual void update() = 0;
  virtual float getCurrentAngle() const = 0;
  virtual float getUpdateDeltaAngle() const = 0;
  virtual float getDeltaAngle() = 0;
  virtual void reset() = 0;
  // virtual float getCurrentAngularSpeed() = 0;
  // virtual float getDeltaAngularSpeed() = 0;
};