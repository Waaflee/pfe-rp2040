#pragma once

#include "interfaces/NonCopyable.hpp"
#include <utility>

class IGyroProcessor : NonCopyable {
  // private:
  //   std::pair<float, float> measure;

public:
  virtual ~IGyroProcessor(){};
  virtual float getAngularSpeed(float rawValue) = 0;
  virtual float getAngle(std::pair<float, float> rawMeasure) = 0;
};
