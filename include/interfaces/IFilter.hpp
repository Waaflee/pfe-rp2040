#pragma once

#include "interfaces/NonCopyable.hpp"

class IFilter : NonCopyable {
public:
  virtual ~IFilter(){};
  virtual float filter(float rawData) = 0;
};