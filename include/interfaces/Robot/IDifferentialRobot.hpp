#pragma once
#include "interfaces/NonCopyable.hpp"

class IDifferentialRobot : NonCopyable {

public:
  virtual void move(const int8_t speed) const = 0;
  virtual void rotate(const int8_t speed) const = 0;
  virtual void stop() const = 0;
};