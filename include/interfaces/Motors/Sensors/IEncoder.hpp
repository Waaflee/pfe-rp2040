#pragma once
#include "interfaces/NonCopyable.hpp"

class IEncoder : NonCopyable {
public:
  virtual void setSteps(int steps) = 0;
  virtual uint getSteps() = 0;
  virtual int getDeltaSteps() = 0;

  virtual float getAngle() = 0;
  virtual float getDeltaAngle() = 0;

  //   virtual float getSpeed() = 0;
  //   virtual float getDeltaSpeed() = 0;
  //   virtual float getAcceleration() = 0;
  //   virtual float getDeltaAcceleration() = 0;

  // virtual void update() = 0;
  virtual void setDirection(bool direction) = 0;
  virtual bool getDirection(bool direction) = 0;
};