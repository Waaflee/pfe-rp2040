#ifndef NATIVE
#include "hardware/pwm.h"
#else

uint pwm_gpio_to_slice_num(uint);
uint pwm_gpio_to_channel(uint);
void pwm_set_enabled(uint, bool);
void pwm_set_clkdiv_int_frac(uint, uint8_t, uint8_t);
void pwm_set_wrap(uint, uint16_t);
void pwm_set_chan_level(uint, uint, uint16_t);

#endif