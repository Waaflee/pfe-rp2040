#pragma once

#ifndef NATIVE
#include "hardware/i2c.h"
#else
void i2c_init(uint *, uint);
void i2c_deinit(uint *);
uint i2c_read_blocking();
uint i2c_write_blocking();
int i2c_write_blocking(i2c_inst_t *, uint8_t, const uint8_t *, size_t, bool);
int i2c_read_blocking(i2c_inst_t *, uint8_t, uint8_t *, size_t, bool);

uint i2c0_value = 0;
uint i2c1_value = 1;
uint *i2c0 = &i2c0_value;
uint *i2c1 = &i2c1_value;
const uint PICO_ERROR_GENERIC = 200;
#endif