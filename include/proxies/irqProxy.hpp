#pragma once

#ifndef NATIVE
#include "hardware/irq.h"
#else
typedef void (*irqHandler)(void);
void irq_set_enabled(uint num, bool enabled);
void irq_set_exclusive_handler(uint num, irqHandler handler);
#endif