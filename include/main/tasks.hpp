
#pragma once

#ifndef NATIVE
#include <FreeRTOS.h>
#include <array>
#include <task.h>

// Tasks Handles
extern TaskHandle_t prvQueueReceiveTaskHandle;
extern TaskHandle_t prvTraslationControlTaskHandle;
extern TaskHandle_t prvRotationControlTaskHandle;
extern TaskHandle_t prvCalibrateSensorsTaskHandle;
extern TaskHandle_t prvProcessIncomingMessageTaskHandle;
extern TaskHandle_t prvReportMovementTaskHandle;

static std::array<TaskHandle_t *, 6> allHandles{
    &prvQueueReceiveTaskHandle,           &prvRotationControlTaskHandle,
    &prvTraslationControlTaskHandle,      &prvCalibrateSensorsTaskHandle,
    &prvProcessIncomingMessageTaskHandle, &prvReportMovementTaskHandle};
;

#endif

enum class TaskHandleID {
  QUEUE_RECEIVE,
  ROTATION_CONTROL,
  TRASLATION_CONTROL,
  CALIBRATE_SENSORS,
  PROCESS_INCOMING_MESSAGE,
  REPORT_MOVEMENT,

};

// Setup Tasks
void vSetupTasks();

// Tasks Utils
void notifyTask(TaskHandleID task);
void notifyTaskFromISR(TaskHandleID task);
