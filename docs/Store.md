# Central Store - Manages System State

The Store is the unique source of truth, available to the whole system. It task is to store the current state of the system and notify subscribed objects of any changes.

This store will be used for the state machine as well as de central communication system among objects.

The store should store:

- current rotation (relative to last rotation)
- current movement (relative to last movement)
- current targeted rotation
- current targeted movement

## State Machine

the system goes through the following states

### Communications

```mermaid
flowchart LR
Idle --> |On new character| process_IRQ
process_IRQ --> |Finished processing IRQ| Idle
process_IRQ --> |On endl char| MessageParser
MessageParser --> N(Notify Central State) -->Idle
```

___
### Movement

```mermaid
flowchart LR

idle --> |uart package arrives|Rotating --> |finished rotation| Moving
Moving --> |reached destination| Calibration
Calibration --> |done recalibrating odometers systems| N(Notify Central Store)
N --> idle

```

#### Rotation

Uses Differential Robot::Rotate(speed) method
Reads Current and Target Rotations from Central Store

```mermaid
flowchart

A(GetsCurrentError) --> PID --> PWM
PWM --> A
A --> |Rotation Finished|B(Notify Central Store)
```

#### Traslation

Uses Differential Robot::Move(speed) method
Reads Current and Target Positions from Central Store

```mermaid
flowchart

A(GetsCurrentError) --> PID --> PWM
PWM --> A
A --> |Traslation Finished|B(Notify Central Store)
```
___
### Odometers

```mermaid
flowchart LR

Idle --> |Timer activates|M(Gets Odometers Data)
M --> M_A(Measures Accelerometer Data)
M --> M_G(Measures Gyroscope Data)
M --> M_RO(Measures Rotational Odometer)
M --> M_OO(Measures Optical Odometer)

M_A --> P_A(Process Accelerometer)
M_G --> P_G(Process Gyroscope)

I_M(Integrate Measurements)

P_A -->I_M
P_G -->I_M
M_RO -->I_M
M_OO -->I_M

I_M --> N_1(Notify Central Store)
I_M --> N_2(Notify Simulation)

N_1 --> Idle
N_2 --> Idle

```

## Diagrams

```mermaid
classDiagram

class Store
Store: -map topics
Store: -float position
Store: -float positionTarget
Store: -float rotation
Store: -float rotationTarget
Store: -State state

Store: +getCurrentPosition()
Store: +getTargetPosition()
Store: +getCurrentRotation()
Store: +getTargetRotation()
Store: +getRotationDiff()
Store: +getPositionDiff()
Store: +resetPosition()
Store: +resetRotation()
Store: +resetRotPos()

Store: +updateTargets()
Store: +updateCurrentRotPos()


Store: +setMovementState()
Store: +getMovementState()
```

## Proposals

### Update Odometer Data and Report to PID Tasks

```mermaid
flowchart LR

RPID(Rotation PID Task)
MPID(Movement PID Task)
S(Central State)
O(Odometers Monitoring)


O -->|Report data|S
S --> |Reactively notifies|RPID
S --> |Reactively notifies|MPID

```

```mermaid
flowchart LR
RPID(Rotation PID Task) --> Setup(Setup: subscribes to data) --> Process --> End(Ends rotation: Unsubscribes)
MPID(Movement PID Task) --> Setup2(Setup: subscribes to data) --> Process2(Process) --> End2(Ends traslation: Unsubscribes)

```
